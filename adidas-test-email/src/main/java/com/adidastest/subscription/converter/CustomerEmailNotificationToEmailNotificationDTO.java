package com.adidastest.subscription.converter;

import com.adidastest.subscription.dto.EmailNotificationDTO;
import com.adidastest.subscription.entity.CustomerEmailNotification;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerEmailNotificationToEmailNotificationDTO implements Converter<CustomerEmailNotification, EmailNotificationDTO> {

    @Override
    public EmailNotificationDTO convert(CustomerEmailNotification customerEmailNotification) {
        EmailNotificationDTO emailNotificationDTO = new EmailNotificationDTO(
                customerEmailNotification.getEmail(),
                customerEmailNotification.getEmailMessage(),
                customerEmailNotification.getCustomerId()
        );
        return emailNotificationDTO;
    }
}
