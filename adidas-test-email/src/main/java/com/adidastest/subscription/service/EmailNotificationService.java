package com.adidastest.subscription.service;

import com.adidastest.subscription.dto.EmailNotificationDTO;
import com.adidastest.subscription.entity.CustomerEmailNotification;
import com.adidastest.subscription.repository.CustomerEmailNotificationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Service
@Validated
public class EmailNotificationService {

    private final CustomerEmailNotificationRepository customerEmailNotificationRepository;
    private final ConversionService conversionService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public EmailNotificationService(final CustomerEmailNotificationRepository customerEmailNotificationRepository,
                                    final ConversionService conversionService){
        this.customerEmailNotificationRepository = customerEmailNotificationRepository;
        this.conversionService = conversionService;
    }

    @Scheduled(cron = "${email.notification.send}")
    public void sendStoredEmailNotification() {
        List<CustomerEmailNotification> storedEmailNotificationList = this.customerEmailNotificationRepository.findAllBySent(Boolean.FALSE);
        storedEmailNotificationList.stream().forEach(emailNotification -> {
            final Boolean emailHasBeenSent = sendEmail(emailNotification);
            if (emailHasBeenSent) {
                emailNotification.setSent(Boolean.TRUE);
                updateEmailNotification(emailNotification);
            }
        });
    }

    public ResponseEntity saveNotification(@Valid final EmailNotificationDTO emailNotificationDTO) {
        CustomerEmailNotification customerEmailNotification = this.conversionService.convert(emailNotificationDTO, CustomerEmailNotification.class);
        this.customerEmailNotificationRepository.save(customerEmailNotification);
        return ResponseEntity.ok().build();
    }

    public CustomerEmailNotification updateEmailNotification(CustomerEmailNotification customerEmailNotification) {
        return this.customerEmailNotificationRepository.save(customerEmailNotification);
    }

    public Boolean sendEmail(CustomerEmailNotification customerEmailNotification) {
        this.logger.debug("Email has been sent successfully");
        return Boolean.TRUE;
    }

    public ResponseEntity<List<EmailNotificationDTO>> readEmailNotification() {
        final List<EmailNotificationDTO> emailNotificationDTOList = new ArrayList<>();
        List<CustomerEmailNotification> emailNotificationList = this.customerEmailNotificationRepository.findAll();
        if (!CollectionUtils.isEmpty(emailNotificationList)) {
            emailNotificationList.stream().forEach(emailNotification -> {
                EmailNotificationDTO emailNotificationDTO = this.conversionService.convert(emailNotification, EmailNotificationDTO.class);
                emailNotificationDTOList.add(emailNotificationDTO);
            });
        }
        return new ResponseEntity<>(emailNotificationDTOList, HttpStatus.OK);
    }
}
