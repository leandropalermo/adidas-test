package com.adidastest.subscription;

import com.adidastest.subscription.config.AccessAspectConfig;
import com.adidastest.subscription.config.ScheduleConfig;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class EmailNotificationAdidasTestApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(
				EmailNotificationAdidasTestApplication.class,
				ScheduleConfig.class,
				AccessAspectConfig.class
		).run(args);
	}

}
