package com.adidastest.subscription.controller;

import com.adidastest.subscription.dto.EmailNotificationDTO;
import com.adidastest.subscription.service.EmailNotificationService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/email-notification")
public class EmailNotificationController {

    private final EmailNotificationService emailNotificationService;

    public EmailNotificationController(final EmailNotificationService emailNotificationService) {
        this.emailNotificationService = emailNotificationService;
    }

    @PostMapping
    @ApiOperation(value = "Send email notification.", response = ResponseEntity.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Email registred succesfully"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 503, message = "The server cannot handle the request"),
    })
    public ResponseEntity saveNotification(@Valid @RequestBody EmailNotificationDTO emailNotificationDTO) {
        return emailNotificationService.saveNotification(emailNotificationDTO);
    }

    @GetMapping
    @ApiOperation(value = "Get All Email Notifications Regitered", response = EmailNotificationDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<EmailNotificationDTO>> readSubscribe() {
        return emailNotificationService.readEmailNotification();
    }
}
