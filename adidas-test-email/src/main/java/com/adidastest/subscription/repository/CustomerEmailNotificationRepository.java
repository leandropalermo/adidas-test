package com.adidastest.subscription.repository;

import com.adidastest.subscription.entity.CustomerEmailNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CustomerEmailNotificationRepository extends JpaRepository<CustomerEmailNotification, Long> {
    List<CustomerEmailNotification> findAllBySent(@Param("sent") Boolean sent);
}
