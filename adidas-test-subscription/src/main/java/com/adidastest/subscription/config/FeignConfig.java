package com.adidastest.subscription.config;

import com.adidastest.subscription.feign.IEmailNotificationFeign;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableFeignClients(basePackageClasses = IEmailNotificationFeign.class)
public class FeignConfig {
}
