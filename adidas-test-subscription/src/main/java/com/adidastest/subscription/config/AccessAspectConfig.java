package com.adidastest.subscription.config;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Configuration;

@Aspect
@Configuration
public class AccessAspectConfig {

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Before("execution(* com.adidastest.subscription.controller.*.*(..))")
    public void beforeAccess(JoinPoint joinPoint){
        logger.info(" Access execution for {}", joinPoint);
    }

    @After("execution(* com.adidastest.subscription.controller.*.*(..))")
    public void afterAccess(JoinPoint joinPoint){
        logger.info(" Access Finalized for {}", joinPoint);
    }
}
