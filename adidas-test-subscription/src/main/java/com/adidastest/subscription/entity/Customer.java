package com.adidastest.subscription.entity;

import com.adidastest.subscription.enumaration.Gender;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Entity
public class Customer {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long customerId;

    @NotNull
    private String email;

    private String firstName;

    @NotNull
    private LocalDate dateOfBirth;

    @NotNull
    private Boolean consent;

    @NotNull
    private Long campaignId;

    private Gender gender;

    public Customer() {

    }

    public Customer( final String email,
                     final String firstName,
                     final LocalDate dateOfBirth,
                     final Boolean consent,
                     final Long campaignId,
                     final Gender gender) {
        this.email = email;
        this.firstName = firstName;
        this.dateOfBirth = dateOfBirth;
        this.consent = consent;
        this.campaignId = campaignId;
        this.gender = gender;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Boolean getConsent() {
        return consent;
    }

    public void setConsent(Boolean consent) {
        this.consent = consent;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    @Override
    public String toString() {
        return "Customer{" +
                "customerId=" + customerId +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", consent=" + consent +
                ", campaignId=" + campaignId +
                ", gender=" + gender +
                '}';
    }
}
