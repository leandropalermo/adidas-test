package com.adidastest.subscription.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Entity
public class CustomerEmailNotification {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;

    @NotNull
    private String email;

    @NotNull
    private String emailMessage;

    @NotNull
    private LocalDateTime dtTimeUpdate;

    @NotNull
    private Long customerId;

    public CustomerEmailNotification(){}

    public CustomerEmailNotification(final String email,
                                     final String emailMessage,
                                     final LocalDateTime dtTimeUpdate,
                                     final Long customerId){
        this.email = email;
        this.emailMessage = emailMessage;
        this.dtTimeUpdate = dtTimeUpdate;
        this.customerId = customerId;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailMessage() {
        return emailMessage;
    }

    public void setEmailMessage(String emailMessage) {
        this.emailMessage = emailMessage;
    }

    public LocalDateTime getDtTimeUpdate() {
        return dtTimeUpdate;
    }

    public void setDtTimeUpdate(LocalDateTime dtTimeUpdate) {
        this.dtTimeUpdate = dtTimeUpdate;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "CustomerEmailNotification{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", emailMessage='" + emailMessage + '\'' +
                ", dtTimeUpdate=" + dtTimeUpdate +
                ", customerId=" + customerId +
                '}';
    }
}
