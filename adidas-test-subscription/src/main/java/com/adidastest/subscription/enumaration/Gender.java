package com.adidastest.subscription.enumaration;

public enum Gender {

    FEMALE, MALE
}
