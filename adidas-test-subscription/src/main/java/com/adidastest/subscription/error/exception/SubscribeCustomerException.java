package com.adidastest.subscription.error.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class SubscribeCustomerException extends RuntimeException{

    public SubscribeCustomerException(String exception) {
        super(exception);
    }

}
