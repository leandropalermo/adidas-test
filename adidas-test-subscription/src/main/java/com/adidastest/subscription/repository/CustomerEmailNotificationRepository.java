package com.adidastest.subscription.repository;

import com.adidastest.subscription.entity.CustomerEmailNotification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerEmailNotificationRepository extends JpaRepository<CustomerEmailNotification, Long> {
}
