package com.adidastest.subscription.converter;

import com.adidastest.subscription.dto.EmailNotificationDTO;
import com.adidastest.subscription.entity.CustomerEmailNotification;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

@Component
public class EmailNotificationDtoToCustomerEmailNotification implements Converter<EmailNotificationDTO, CustomerEmailNotification> {

    @Override
    public CustomerEmailNotification convert(EmailNotificationDTO emailNotificationDTO) {
        CustomerEmailNotification customerEmailNotification = new CustomerEmailNotification(
                emailNotificationDTO.getEmail(),
                emailNotificationDTO.getEmailMessage(),
                LocalDateTime.now(),
                emailNotificationDTO.getCustomerId()
        );
        return customerEmailNotification;
    }
}
