package com.adidastest.subscription.converter;

import com.adidastest.subscription.dto.CustomerDTO;
import com.adidastest.subscription.entity.Customer;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerDtoToCustomerConverter implements Converter<CustomerDTO, Customer> {

    @Override
    public Customer convert(CustomerDTO customerDTO) {
        Customer customer = new Customer(
                customerDTO.getEmail(),
                customerDTO.getFirstName(),
                customerDTO.getDateOfBirth(),
                customerDTO.getConsent(),
                customerDTO.getCampaignId(),
                customerDTO.getGender()
        );

        return customer;
    }
}
