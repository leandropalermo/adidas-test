package com.adidastest.subscription.converter;

import com.adidastest.subscription.dto.CustomerDTO;
import com.adidastest.subscription.entity.Customer;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerToCustomerDTO implements Converter<Customer, CustomerDTO> {

    @Override
    public CustomerDTO convert(Customer customer) {
        CustomerDTO customerDTO = new CustomerDTO(
                customer.getCustomerId(),
                customer.getEmail(),
                customer.getFirstName(),
                customer.getDateOfBirth(),
                customer.getConsent(),
                customer.getCampaignId(),
                customer.getGender()
        );
        return customerDTO;
    }
}
