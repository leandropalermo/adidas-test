package com.adidastest.subscription.feign;

import com.adidastest.subscription.dto.CustomerDTO;
import com.adidastest.subscription.dto.EmailNotificationDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "IEmailNotificationFeign", url = "${email.notification.url}")
public interface IEmailNotificationFeign {

    @PostMapping
    ResponseEntity emailNotification(final EmailNotificationDTO emailNotificationDTO);
}
