package com.adidastest.subscription.service;

import com.adidastest.subscription.dto.EmailNotificationDTO;
import com.adidastest.subscription.entity.CustomerEmailNotification;
import com.adidastest.subscription.feign.IEmailNotificationFeign;
import com.adidastest.subscription.repository.CustomerEmailNotificationRepository;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import java.util.List;

@Service
@Validated
public class EmailNotificationService {

    private final IEmailNotificationFeign emailNotificationFeign;
    private final CustomerEmailNotificationRepository customerEmailNotificationRepository;
    private final ConversionService conversionService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public EmailNotificationService(final IEmailNotificationFeign emailNotificationFeign,
                                    final CustomerEmailNotificationRepository customerEmailNotificationRepository,
                                    final ConversionService conversionService){
        this.emailNotificationFeign = emailNotificationFeign;
        this.customerEmailNotificationRepository = customerEmailNotificationRepository;
        this.conversionService = conversionService;
    }

    @HystrixCommand(fallbackMethod = "sendEmailNotificationFallback")
    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "100")
    public HttpStatus sendEmailNotification(@Valid final EmailNotificationDTO emailNotificationDTO) {
        ResponseEntity emailNotificationResponseEntity = this.emailNotificationFeign.emailNotification(emailNotificationDTO);
        if (!emailNotificationResponseEntity.getStatusCode().equals(HttpStatus.OK)) {
            this.logger.debug("Not sent email: {}", emailNotificationDTO.getEmail());
        }
        return emailNotificationResponseEntity.getStatusCode();
    }

    public HttpStatus sendEmailNotificationFallback(final EmailNotificationDTO emailNotificationDTO) {
        HttpStatus response = HttpStatus.SERVICE_UNAVAILABLE;
        CustomerEmailNotification customerEmailNotification = this.conversionService.convert(emailNotificationDTO, CustomerEmailNotification.class);
        try {
            save(customerEmailNotification);
        } catch (Exception e) {
            this.logger.error("Error in saving EmailNotification: {}", e.getMessage());
            response = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return response;
    }



    public void deleteStoredEmailNotification(CustomerEmailNotification customerEmailNotification) {
        this.customerEmailNotificationRepository.delete(customerEmailNotification);
    }

    public List<CustomerEmailNotification> findAll() {
        return this.customerEmailNotificationRepository.findAll();
    }

    public CustomerEmailNotification save(CustomerEmailNotification customerEmailNotification) {
        return this.customerEmailNotificationRepository.save(customerEmailNotification);
    }
}
