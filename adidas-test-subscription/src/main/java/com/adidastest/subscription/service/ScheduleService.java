package com.adidastest.subscription.service;

import com.adidastest.subscription.dto.EmailNotificationDTO;
import com.adidastest.subscription.entity.CustomerEmailNotification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScheduleService {

    private final EmailNotificationService emailNotificationService;
    private final ConversionService conversionService;

    public ScheduleService(final EmailNotificationService emailNotificationService, final ConversionService conversionService) {
        this.emailNotificationService = emailNotificationService;
        this.conversionService = conversionService;
    }

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @Scheduled(cron = "${email.notification.re-send}")
    public void sendStoredEmailNotification() {
        List<CustomerEmailNotification> storedEmailNotificationList = this.emailNotificationService.findAll();
        storedEmailNotificationList.stream().forEach(emailNotification -> {
            this.logger.debug("Resending emailNotification: {}", emailNotification);
            EmailNotificationDTO emailNotificationDTO = this.conversionService.convert(emailNotification, EmailNotificationDTO.class);
            HttpStatus httpStatus = this.emailNotificationService.sendEmailNotification(emailNotificationDTO);
            this.emailNotificationService.deleteStoredEmailNotification(emailNotification);
        });
    }
}
