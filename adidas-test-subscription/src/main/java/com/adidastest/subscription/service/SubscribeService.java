package com.adidastest.subscription.service;

import com.adidastest.subscription.dto.CustomerDTO;
import com.adidastest.subscription.entity.Customer;
import com.adidastest.subscription.entity.CustomerEmailNotification;
import com.adidastest.subscription.error.exception.SubscribeCustomerException;
import com.adidastest.subscription.repository.CustomerRepository;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Service
public class SubscribeService {


    private final CustomerRepository customerRepository;
    private final ConversionService conversionService;
    private final EmailNotificationService emailNotificationService;

    private static String EMAIL_NOTIFICATION_SUBSCRIBE_MESSAGE = "Your subscription has been successfully completed.";

    public SubscribeService(final CustomerRepository customerRepository,
                            final ConversionService conversionService,
                            final EmailNotificationService emailNotificationService){
        this.customerRepository = customerRepository;
        this.conversionService = conversionService;
        this.emailNotificationService = emailNotificationService;
    }


    public ResponseEntity<CustomerDTO> subscribeCustomer(final CustomerDTO customerDTO) throws SubscribeCustomerException {
        Customer customer = this.conversionService.convert(customerDTO, Customer.class);
        customer = saveCustomer(customer);
        saveCustomerEmailNotification(customer);
        CustomerDTO responseBody = this.conversionService.convert(customer, CustomerDTO.class);
        return new ResponseEntity<>(responseBody, HttpStatus.CREATED);
    }

    private void saveCustomerEmailNotification(Customer customer) {
        CustomerEmailNotification customerEmailNotification = new CustomerEmailNotification(
                customer.getEmail(),
                SubscribeService.EMAIL_NOTIFICATION_SUBSCRIBE_MESSAGE,
                LocalDateTime.now(),
                customer.getCustomerId()
        );
        this.emailNotificationService.save(customerEmailNotification);
    }

    private Customer saveCustomer(Customer customer) throws SubscribeCustomerException {
        try {
            customer = this.customerRepository.save(customer);
        } catch (Exception e) {
            throw new SubscribeCustomerException("Error on subscribing customer");
        }
        return customer;
    }

    public ResponseEntity<List<CustomerDTO>> readSubscribe() {
        final List<CustomerDTO> customerDTOList = new ArrayList<>();
        List<Customer> customerList = this.customerRepository.findAll();
        if (!CollectionUtils.isEmpty(customerList)) {
            customerList.stream().forEach(customer -> {
                CustomerDTO customerDTO = this.conversionService.convert(customer, CustomerDTO.class);
                customerDTOList.add(customerDTO);
            });
        }
        return new ResponseEntity<>(customerDTOList, HttpStatus.OK);
    }
}
