package com.adidastest.subscription;

import com.adidastest.subscription.config.AccessAspectConfig;
import com.adidastest.subscription.config.FeignConfig;
import com.adidastest.subscription.config.HystrixConfig;
import com.adidastest.subscription.config.ScheduleConfig;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class SubscriptionAdidasTestApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(
				SubscriptionAdidasTestApplication.class,
				FeignConfig.class,
				HystrixConfig.class,
                ScheduleConfig.class,
				AccessAspectConfig.class
		).run(args);
	}

}
