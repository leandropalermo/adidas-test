package com.adidastest.subscription.dto;

import javax.validation.constraints.NotNull;

public class EmailNotificationDTO {

    @NotNull(message = "Email cannot be null")
    private String email;

    @NotNull(message = "Email message cannot be null")
    private String emailMessage;

    @NotNull(message = "CustomerId cannot be null")
    private Long customerId;

    public EmailNotificationDTO() {

    }

    public EmailNotificationDTO(final String email, final String emailMessage, final Long customerId) {
        this.email = email;
        this.emailMessage = emailMessage;
        this.customerId = customerId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmailMessage() {
        return emailMessage;
    }

    public void setEmailMessage(String emailMessage) {
        this.emailMessage = emailMessage;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "EmailNotificationDTO{" +
                "email='" + email + '\'' +
                ", emailMessage='" + emailMessage + '\'' +
                ", customerId='" + customerId + '\'' +
                '}';
    }
}
