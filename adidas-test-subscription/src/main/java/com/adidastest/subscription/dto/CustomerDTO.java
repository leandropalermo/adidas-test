package com.adidastest.subscription.dto;

import com.adidastest.subscription.enumaration.Gender;

import javax.validation.constraints.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class CustomerDTO {

    private Long customerId;

    @NotEmpty(message= "Email cannot be empty")
    @Email(message = "Email should be valid")
    private String email;

    private String firstName;

    @Past(message = "Date of Birth should be before today")
    @NotNull(message = "Date of Birth cannot be null")
    private LocalDate dateOfBirth;

    @NotNull(message = "Consent cannot be null")
    private Boolean consent;

    @Positive(message = "Campaign Id must be informed.")
    @NotNull(message = "Campaign Id cannot be null")
    private Long campaignId;

    private Gender gender;

    public CustomerDTO() {}

    public CustomerDTO( final Long customerId,
                        final String email,
                        final String firstName,
                        final LocalDate dateOfBirth,
                        final Boolean consent,
                        final Long campaignId,
                        final Gender gender) {
        this.customerId = customerId;
        this.email = email;
        this.firstName = firstName;
        this.dateOfBirth = dateOfBirth;
        this.consent = consent;
        this.campaignId = campaignId;
        this.gender = gender;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public LocalDate getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(LocalDate dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Boolean getConsent() {
        return consent;
    }

    public void setConsent(Boolean consent) {
        this.consent = consent;
    }

    public Long getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(Long campaignId) {
        this.campaignId = campaignId;
    }

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    @Override
    public String toString() {
        return "CustomerDTO{" +
                "customerId=" + customerId +
                ", email='" + email + '\'' +
                ", firstName='" + firstName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", consent=" + consent +
                ", campaignId=" + campaignId +
                ", gender=" + gender +
                ", customerId=" + customerId +
                '}';
    }
}
