package com.adidastest.enumaration;

public enum Gender {

    FEMALE, MALE
}
