package com.adidastest.converter;

import com.adidastest.dto.CustomerDTO;
import com.adidastest.entity.Customer;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerDtoToCustomerConverter implements Converter<CustomerDTO, Customer> {

    private final Long SUBSCRIBE_ID = 0L;

    @Override
    public Customer convert(CustomerDTO customerDTO) {
        Customer customer = new Customer(
                customerDTO.getEmail(),
                customerDTO.getFirstName(),
                customerDTO.getDateOfBirth(),
                customerDTO.getConsent(),
                customerDTO.getCampaignId(),
                customerDTO.getGender(),
                customerDTO.getCustomerId()
        );
        return customer;
    }
}
