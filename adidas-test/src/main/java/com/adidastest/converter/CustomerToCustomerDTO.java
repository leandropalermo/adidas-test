package com.adidastest.converter;

import com.adidastest.dto.CustomerDTO;
import com.adidastest.entity.Customer;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class CustomerToCustomerDTO implements Converter<Customer, CustomerDTO> {

    @Override
    public CustomerDTO convert(Customer customer) {
        CustomerDTO customerDTO = new CustomerDTO(
                customer.getEmail(),
                customer.getFirstName(),
                customer.getDateOfBirth(),
                customer.getConsent(),
                customer.getCampaignId(),
                customer.getGender(),
                customer.getCustomerId()
        );
        return customerDTO;
    }
}
