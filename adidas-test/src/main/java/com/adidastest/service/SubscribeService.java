package com.adidastest.service;

import com.adidastest.dto.CustomerDTO;
import com.adidastest.entity.Customer;
import com.adidastest.error.exception.CustomerNotSavedException;
import com.adidastest.error.exception.CustomerSubscribeException;
import com.adidastest.feign.ISubscribeFeign;
import com.adidastest.repository.CustomerRepository;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class SubscribeService {

    private final ISubscribeFeign iSubscribeFeign;
    private final ConversionService conversionService;
    private final CustomerRepository customerRepository;

    private Logger logger = LoggerFactory.getLogger(this.getClass());


    public SubscribeService(final ISubscribeFeign iSubscribeFeign,
                            final ConversionService conversionService,
                            final CustomerRepository customerRepository){
        this.iSubscribeFeign = iSubscribeFeign;
        this.conversionService = conversionService;
        this.customerRepository = customerRepository;
    }

    @HystrixCommand(fallbackMethod = "subscribeUserFallback")
    @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "100")
    public ResponseEntity<CustomerDTO> subscribeCustomer(final CustomerDTO customerDTO) {
        if (ObjectUtils.isEmpty(customerDTO)) {
            throw new CustomerSubscribeException("DTO cannot be null.");
        }
        ResponseEntity<CustomerDTO> customerDTOResponseEntity = iSubscribeFeign.subscribeCustomer(customerDTO);
        return new ResponseEntity<>(customerDTOResponseEntity.getBody(), HttpStatus.CREATED);
    }

    public ResponseEntity<CustomerDTO> subscribeUserFallback(final CustomerDTO customerDTO) {
        throw new CustomerSubscribeException("Error on user subscribe.");
    }

    public HttpStatus convertAndSaveCustomer(CustomerDTO customerDTO) {
        Customer customer = this.conversionService.convert(customerDTO, Customer.class);
        save(customer);
        return HttpStatus.CREATED;
    }

    public HttpStatus convertAndSaveCustomer(final CustomerDTO customerDTO, final Long id) {
        Customer customer = this.conversionService.convert(customerDTO, Customer.class);
        customer.setId(id);
        save(customer);
        return HttpStatus.CREATED;
    }

    public Customer save(Customer customer) {
        try {
            customer = this.customerRepository.save(customer);
        } catch (Exception e) {
            throw new CustomerNotSavedException("The server could not subscribe the customer");
        }
        return customer;
    }

    public List<Customer> findByCustomerId(final Long customerId) {
        List<Customer> customerList = this.customerRepository.findByCustomerId(customerId);
        if (ObjectUtils.isEmpty(customerList)) {
            customerList = Collections.EMPTY_LIST;
        }
        return customerList;
    }

    public ResponseEntity<List<CustomerDTO>> readSubscribe() {
        final List<CustomerDTO> customerDTOList = new ArrayList<>();
        List<Customer> customerList = this.customerRepository.findAll();
        if (!CollectionUtils.isEmpty(customerList)) {
            customerList.stream().forEach(customer -> {
                CustomerDTO customerDTO = this.conversionService.convert(customer, CustomerDTO.class);
                customerDTOList.add(customerDTO);
            });
        }
        return new ResponseEntity<>(customerDTOList, HttpStatus.OK);
    }
}
