package com.adidastest.service;

import com.adidastest.dto.CustomerDTO;
import com.adidastest.entity.Customer;
import com.adidastest.error.exception.CustomerSubscribeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.convert.ConversionService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ScheduleService {

    private final SubscribeService subscribeService;
    private final ConversionService conversionService;

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    public ScheduleService(final SubscribeService subscribeService, final ConversionService conversionService) {
        this.subscribeService = subscribeService;
        this.conversionService = conversionService;
    }

    @Scheduled(cron = "${subscribe.customer.schedule}")
    public void subscribeCustomerProcess() {
        final Long subscribeIdZero = 0L;
        List<Customer> customerList = this.subscribeService.findByCustomerId(subscribeIdZero);
        customerList.stream().forEach(customer -> {
            CustomerDTO customerDTO = this.conversionService.convert(customer, CustomerDTO.class);
            try {
                ResponseEntity<CustomerDTO> customerDTOResponseEntity = this.subscribeService.subscribeCustomer(customerDTO);
                if (customerDTOResponseEntity.getStatusCode().equals(HttpStatus.CREATED)) {
                    this.subscribeService.convertAndSaveCustomer(customerDTOResponseEntity.getBody(), customer.getId());
                }
            } catch (CustomerSubscribeException e) {
                this.logger.debug(e.getMessage());
            }
        });
    }
}
