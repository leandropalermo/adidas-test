package com.adidastest.feign;

import com.adidastest.dto.CustomerDTO;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;

@FeignClient(name = "ISubscribeFeign", url = "${subscribe.customer.url}")
public interface ISubscribeFeign {

    @PostMapping
    ResponseEntity<CustomerDTO> subscribeCustomer(final CustomerDTO customerDTO);
}
