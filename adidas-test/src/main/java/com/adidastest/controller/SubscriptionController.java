package com.adidastest.controller;

import com.adidastest.dto.CustomerDTO;
import com.adidastest.service.SubscribeService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/public/subscribe")
public class SubscriptionController {

    private final SubscribeService subscribeService;

    public SubscriptionController(final SubscribeService subscribeService) {
        this.subscribeService = subscribeService;
    }

    @PostMapping
    @ApiOperation(value = "Subscribe a new user.", response = CustomerDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 201, message = "Successfully subscribed"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 503, message = "The server cannot handle the request"),
            @ApiResponse(code = 504, message = "Gateway Timeout")
    })
    public HttpStatus subscribeUser(@Valid @RequestBody CustomerDTO customerDTO) {
        if (customerDTO.getCustomerId() != 0) {
            customerDTO.setCustomerId(0L);
        }
        return subscribeService.convertAndSaveCustomer(customerDTO);
    }


    @GetMapping
    @ApiOperation(value = "Get All Customer regitered", response = CustomerDTO.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    public ResponseEntity<List<CustomerDTO>> readSubscribe() {
        return subscribeService.readSubscribe();
    }
}
