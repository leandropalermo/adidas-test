package com.adidastest.error;

import com.adidastest.error.exception.CustomerSubscribeException;
import org.apache.commons.lang.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

@ControllerAdvice
@RestController
public class CustomizedResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(CustomerSubscribeException.class)
    public final ResponseEntity<ErrorResponse> handleClientSubscribeException(final CustomerSubscribeException ex, final WebRequest request) {
        final List<String> errorMessages = new ArrayList<>();
        errorMessages.add(ex.getMessage());
        final ErrorResponse errorResponse = new ErrorResponse(LocalDateTime.now(), errorMessages,
                request.getDescription(false));
        return new ResponseEntity<>(errorResponse, HttpStatus.SERVICE_UNAVAILABLE);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {

        final List<String> errorMessages = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());
        final ErrorResponse errorDetails = new ErrorResponse(LocalDateTime.now(), errorMessages,"Invalid RequestBody.");
        return new ResponseEntity<>(errorDetails, status);
    }

    @ExceptionHandler(TimeoutException.class)
    public final ResponseEntity<ErrorResponse> HandleTimeoutException(final TimeoutException ex) {
        final String timeoutMessage = "Gateway timeout";
        final List<String> errorMessages = new ArrayList<>();
        errorMessages.add(timeoutMessage);
        final ErrorResponse errorResponse = new ErrorResponse(LocalDateTime.now(), errorMessages, StringUtils.EMPTY);
        return new ResponseEntity<>(errorResponse, HttpStatus.GATEWAY_TIMEOUT);
    }
}
