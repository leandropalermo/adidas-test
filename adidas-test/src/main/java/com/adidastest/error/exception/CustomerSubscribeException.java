package com.adidastest.error.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.CONFLICT)
public class CustomerSubscribeException extends RuntimeException{

    public CustomerSubscribeException(String exception) {
        super(exception);
    }

}
