package com.adidastest;

import com.adidastest.config.AccessAspectConfig;
import com.adidastest.config.FeignConfig;
import com.adidastest.config.HystrixConfig;
import com.adidastest.config.ScheduleConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class AdidasTestApplication {

	public static void main(String[] args) {
		new SpringApplicationBuilder(
				AdidasTestApplication.class,
				FeignConfig.class,
				HystrixConfig.class,
				ScheduleConfig.class,
				AccessAspectConfig.class
		).run(args);
	}

}
